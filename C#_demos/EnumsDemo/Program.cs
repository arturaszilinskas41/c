﻿using System;
using System.Collections.Generic;
namespace EnumsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create a collection to store employees
            List<Employee> empList = new List<Employee>();
            empList.Add(new Employee() { Name = "Anurag", Gender = (int)Gender.Female });
            empList.Add(new Employee() { Name = "Pranaya", Gender = 1 });
            empList.Add(new Employee() { Name = "Priyanka", Gender = 2 });
            empList.Add(new Employee() { Name = "Sambit", Gender = 3 });

            //Loop through each employees and print the Name and Gender
            foreach (var emp in empList)
            {
                Console.WriteLine("Name = {0} && Gender = {1}", emp.Name, GetGender(emp.Gender));
            }
            

            int[] Values = (int[])Enum.GetValues(typeof(Gender));

            foreach (var value in Values) {
                Console.WriteLine(value);
                Console.WriteLine(GetGender(value));
            }

            string[] names = (string[])Enum.GetNames(typeof(Gender));

            foreach(string name in names)
            {
                Console.WriteLine(name);
            }

            Console.ReadLine();
        }
        //This method is used to return the Gender 
        public static string GetGender(int gender)
        {
            // The switch here is now more readable and maintainable because 
            // of replacing the integral numbers with Gender enum
            switch (gender)
            {
                case (int)Gender.Unknown:
                    return "Unknown";
                case (int)Gender.Male:
                    return "Male";
                case (int)Gender.Female:
                    return "Female";
                default:
                    return "Invalid Data for Gender";
            }
        }
    }
    // 0 - Unknown
    // 1 - Male
    // 2 - Female
    public class Employee
    {
        public string Name { get; set; }
        public int Gender { get; set; }
    }
    public enum Gender : int
    {
        Unknown = 2,
        Male,
        Female
    }
}