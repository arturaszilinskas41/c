﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousDeligateMethod
{
    public class AnonymousMethods
    {
        public delegate string GreetingsDelegate(string name);

        static void Main(string[] args)
        {
            string Message = "Welcome to Dotnet Tutorials";
            GreetingsDelegate gd = delegate (string name)
            {
                Console.WriteLine("Invoked 1") ;
                return "Hello @" + name + " " + Message;
            };

            gd += delegate (string name)
            {
                Console.WriteLine("Invoked 2");
                return "2.Hello @" + name + " " + Message;
            };

            string GreetingsMessage = gd.Invoke("Pranaya");
            Console.WriteLine(GreetingsMessage);
            Console.ReadKey();
        }
    }
}
