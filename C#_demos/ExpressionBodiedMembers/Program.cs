﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionBodiedMembers
{
    public class Sports
    {
        private string[] types = {"Cricket", "Baseball", "Basketball", "Football",
                              "Hockey", "Soccer", "Tennis","Volleyball" };
        public string this[int i]
        {
            get => types[i];
            set => types[i] = value;
        }
    }
    class Program
    {
        static void Main()
        {
            Sports sports = new Sports();
            Console.WriteLine(sports[0]);
            Console.WriteLine(sports[2]);
            Console.WriteLine("Press any key to exists");
            Console.ReadKey();
        }

    }
}
