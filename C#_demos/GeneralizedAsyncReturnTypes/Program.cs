﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralizedAsyncReturnTypes
{
    public class Example
    {
        public static void Main()
        {
            RunCounter().Wait();
            Console.WriteLine("Press any key to exist.");
            Console.ReadKey();
        }
        private static async Task RunCounter()
        {
            var count = new Counter(2);
            await count.StartCounting(8);
        }
    }
    public class Counter
    {
        private int threshold = 0;
        private int iterations = 0;
        private int ctr = 0;
        event EventHandler<EventArgs> ThresholdReached;
        public Counter(int threshold)
        {
            this.threshold = threshold;
            ThresholdReached += thresholdReachedEvent;
        }
        public async Task<int> StartCounting(int limit)
        {
            iterations = 1;
            for (int index = 0; index <= limit; index++)
            {
                if (ctr == threshold)
                    thresholdReachedEvent(this, EventArgs.Empty);
                ctr++;
                Console.WriteLine($"ctr {ctr}");
                await Task.Delay(500);
            }
            int retval = ctr + (iterations - 1) * threshold;
            Console.WriteLine($"On iteration {iterations}, reached {limit}");
            return retval;
        }
        async void thresholdReachedEvent(object sender, EventArgs e)
        {
            Console.WriteLine($"Reached {ctr}. Resetting...");
            await Task.Delay(1000);
            ctr = 0;
            iterations++;
        }
    }
}
