﻿using System;
using AutoMapper;

namespace AutoMapperDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Step1: Call the InitializeAutomapper method to initialize the Mapper
            //In InitializeAutomapper method we defined the Mappings
            Mapper mapper = InitializeAutomapper();
            //Step2: Create and populate the Employee object
            Address empAddres = new Address()
            {
                City = "Mumbai",
                Stae = "Maharashtra",
                Country = "India"
            };
            Employee emp = new Employee();
            emp.Name = "James";
            emp.Salary = 20000;
            emp.Department = "IT";
            emp.address = empAddres;

            //Step3: Use the mapper to map the employee data with the Employee DTO
            var empDTO = mapper.Map<Employee, EmployeeDTO>(emp);

            Console.WriteLine("Name:" + empDTO.Name + ", Salary:" + empDTO.Salary + ", Department:" + empDTO.Department);
            Console.WriteLine("CityDTO:" + empDTO.addressDTO.CityDTO + ", State:" + empDTO.addressDTO.Stae + ", Country:" + empDTO.addressDTO.Country);
            Console.WriteLine("INFO:" + empDTO.infoStringDTO );
            Console.ReadLine();
        }
        static Mapper InitializeAutomapper()
        {
            var config = new MapperConfiguration(cfg => { 
                cfg.CreateMap<Address, AddressDTO>().ForMember(dest => dest.CityDTO, act => act.MapFrom(src => src.City));
                cfg.CreateMap<Employee, EmployeeDTO>()

                    .ForMember(dest => dest.addressDTO, act => act.MapFrom(src => src.address))

                    .ForMember(dest => dest.infoStringDTO, act => act.MapFrom(src => new EmployeeDTO()
                    {
                        infoStringDTO = src.Info.infoString
                    }));

                }

            );
            var mapper = new Mapper(config);
            return mapper;
        }
    }


    public class Employee
    {
        public string Name { get; set; }
        public int Salary { get; set; }
        public string Department { get; set; }
        public Address address { get; set; }
        public Info Info { get; set; }

    }
    public class EmployeeDTO
    {
        public string Name { get; set; }
        public int Salary { get; set; }
        public string Department { get; set; }
        public AddressDTO addressDTO { get; set; }
        public string infoStringDTO { get; set; }
       
    }
    public class Address
    {
        public string City { get; set; }
        public string Stae { get; set; }
        public string Country { get; set; }
    }
    public class AddressDTO
    {
        public string CityDTO { get; set; }
        public string Stae { get; set; }
        public string Country { get; set; }
    }

    public class Info
    {
        public string infoString { get; set; }
    }
}