﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticNonStaticDemo
{
    class Example
    {
        int x; // Non statuc variable
        static int y = 200; //Static Variable
        public Example(int x)
        {
            this.x = x;
        }
        static void Main(string[] args)
        {
            //Accessing the static variable using class name
            //Before object creation
            Console.WriteLine("Static Variable Y = " + Example.y);
            //Creating object1
            Example obj1 = new Example(50);
            //Creating object2
            Example obj2 = new Example(100);
            Console.WriteLine($"object1 x = {obj1.x}  object2 x = {obj2.x}");
            Console.WriteLine("Press any key to exit.");
            Console.ReadLine();
        }
    }
}
