﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WelcomeToCSharp
{
    class StockPortfolio
    {
        private List<float> stocks;

        public StockPortfolio()
        {
            this.stocks = new List<float>();
        }

        public void AddStock(float stock) {
            this.stocks.Add(stock);
        }
    }
}
