﻿using System;
using System.Threading;
using System.Threading.Tasks;
namespace TaskBasedAsynchronousProgramming
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Main Thread : {Thread.CurrentThread.ManagedThreadId} Statred");

            //Task task1 = new Task(PrintCounter);
            //task1.Start();

            //Task task1 = Task.Run(() => { PrintCounter(); });

            //Task task1 = Task.Factory.StartNew(PrintCounter);

            //Task task1 = Task.Run(() =>
            //{
            //    PrintCounter();
            //});
            //task1.Wait();

            //Console.WriteLine($"Main Thread : {Thread.CurrentThread.ManagedThreadId} Completed");

            Task<double> task1 = Task.Run(() =>
            {
                return CalculateSum(10);
            });

            Console.WriteLine($"Sum is: {task1.Result}");
            Console.WriteLine($"Main Thread Completed");
            Console.ReadKey();
        }
        static void PrintCounter()
        {
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Started");
            for (int count = 1; count <= 5; count++)
            {
                Console.WriteLine($"count value: {count}");
            }
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
        }

        static double CalculateSum(int num)
        {
            double sum = 0;
            for (int count = 1; count <= num; count++)
            {
                sum += count;
            }
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
            return sum;
        }

    }
}