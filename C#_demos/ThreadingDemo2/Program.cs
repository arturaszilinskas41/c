﻿using System.Threading;
using System;
namespace ThreadingDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            int Max = 10;//looks like string[] args
            NumberHelper obj = new NumberHelper(Max);// looks like main()

            Thread T1 = new Thread(new ThreadStart(obj.DisplayNumbers));

            T1.Start(); //looks like main tread start
            Console.Read();
        }
    }
    public class NumberHelper
    {
        int _Number;

        public NumberHelper(int Number)
        {
            _Number = Number;
        }

        public void DisplayNumbers()
        {
            for (int i = 1; i <= _Number; i++)
            {
                Console.WriteLine("value : " + i);
            }
        }
    }

}
