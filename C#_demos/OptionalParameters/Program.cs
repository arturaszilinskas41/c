﻿using System;
namespace OptionalParameter
{
    class Program
    {
        static void Main(string[] args)
        {
            ADDNumbers(10, 20);
            ADDNumbers(10, 20, 30, 40);
            ADDNumbers(10, 20, new int[] { 30, 40, 50 });
            ADDNumbers(FN: 10, SN:10 , new int[] { 30, 40, 50 });
            Console.ReadLine();
        }
        public static void ADDNumbers(int FN, int SN = 1, params int[] restOfTheNumbers)
        {
            int result = FN + SN;
            foreach (int i in restOfTheNumbers)
            {
                result += i;
            }
            Console.WriteLine("(Optional params)Total = " + result.ToString());
        }


        public static void ADDNumbers(int FN, int SN)
        {
            int result = FN + SN;
            Console.WriteLine("(Overload)Total = " + result.ToString());
        }

    }
}
