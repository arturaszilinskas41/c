﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace IndexersDemo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Company company = new Company();
            Console.WriteLine("Name of Employee with Id = 101: " + company[101]);
            Console.WriteLine();
            Console.WriteLine("Name of Employee with Id = 105: " + company[105]);
            Console.WriteLine();
            Console.WriteLine("Name of Employee with Id = 107: " + company[107]);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Changing the names of employees with Id = 101,105,107");
            Console.WriteLine();
            company[101] = "Employee 101 Name Changed";
            company[105] = "Employee 105 Name Changed";
            company[107] = "Employee 107 Name Changed";
            Console.WriteLine("Name of Employee with Id = 101: " + company[101]);
            Console.WriteLine();
            Console.WriteLine("Name of Employee with Id = 105: " + company[105]);
            Console.WriteLine();
            Console.WriteLine("Name of Employee with Id = 107: " + company[107]);

            Console.WriteLine();
            Console.WriteLine("males count: " + company["male"]);
            Console.WriteLine();
            Console.WriteLine("females count: " + company["female"]);
            Console.ReadLine();
        }
    }

    public class Employee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public double Salary { get; set; }
    }

    public class Company
    {
        //Create a varibale to hold a list of employees
        private List<Employee> listEmployees;
        //Through the constructor initialize the listEmployees variable
        public Company()
        {
            listEmployees = new List<Employee>();
            listEmployees.Add(new Employee
            { EmployeeId = 101, Name = "Pranaya", Gender = "Male", Salary = 1000 });
            listEmployees.Add(new Employee
            { EmployeeId = 102, Name = "Preety", Gender = "Female", Salary = 2000 });
            listEmployees.Add(new Employee
            { EmployeeId = 103, Name = "Anurag", Gender = "Male", Salary = 5000 });
            listEmployees.Add(new Employee
            { EmployeeId = 104, Name = "Priyanka", Gender = "Female", Salary = 4000 });
            listEmployees.Add(new Employee
            { EmployeeId = 105, Name = "Hina", Gender = "Female", Salary = 3000 });
            listEmployees.Add(new Employee
            { EmployeeId = 106, Name = "Sambit", Gender = "Male", Salary = 6000 });
            listEmployees.Add(new Employee
            { EmployeeId = 107, Name = "Tarun", Gender = "Male", Salary = 8000 });
            listEmployees.Add(new Employee
            { EmployeeId = 108, Name = "Santosh", Gender = "Male", Salary = 7000 });
            listEmployees.Add(new Employee
            { EmployeeId = 109, Name = "Trupti", Gender = "Female", Salary = 5000 });
        }

        // The indexer takes an employeeId as parameter
        // and returns the employee name
        public string this[int employeeId]
        {
            get
            {
                return listEmployees.
                    FirstOrDefault(x => x.EmployeeId == employeeId).Name;
            }
            set
            {
                listEmployees.
                    FirstOrDefault(x => x.EmployeeId == employeeId).Name = value;
            }
        }

        public string this[string gender]
        {
            get
            {
                return listEmployees.Count((x) => x.Gender.ToLower() == gender.ToLower()).ToString();
            }
            //set
            //{
            //    listEmployees.
            //        FirstOrDefault(x => x.EmployeeId == employeeId).Name = value;
            //}
        }
    }
}