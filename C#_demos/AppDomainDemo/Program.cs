﻿using System;
using System.Security;
using System.Security.Permissions;
namespace AppDomainDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create Permission object
            var permission = new PermissionSet(PermissionState.None);
            permission.AddPermission(
                new SecurityPermission(SecurityPermissionFlag.Execution)
                );
            //Set No Access to C drive
            permission.AddPermission(
               new FileIOPermission(FileIOPermissionAccess.NoAccess, @"D:\")
               );
            //Create setup for App Domain
            var setUp = new AppDomainSetup();
            setUp.ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            //Create custom App Domain using the setup and permission
            AppDomain customDomain = AppDomain.CreateDomain("customDomain", null, setUp, permission);
            try
            {
                //Get the Type of ThirdParty
                Type thirdParty = typeof(ThirdParty);
                //Create object of ThirdParty using customDomain
                Console.WriteLine("Create object of ThirdParty using customDomain");
                customDomain.CreateInstanceAndUnwrap(
                                      thirdParty.Assembly.FullName,
                                      thirdParty.FullName);
            }
            catch (Exception ex)
            {
                //Unload the domain
                Console.WriteLine(ex.Message);
                AppDomain.Unload(customDomain);
            }

            //Own DLL
            MyClass1 obj1 = new MyClass1();
            MyClass2 obj2 = new MyClass2();
            Console.Read();
        }
    }
    [Serializable]
    public class ThirdParty
    {
        public ThirdParty()
        {
            Console.WriteLine("Third Party DLL Loaded");
            System.IO.File.Create(@"C:\xyz.txt");
        }
        ~ThirdParty()
        {
            Console.WriteLine("Third Party DLL Unloaded");
        }
    }

    public class MyClass1
    {
    }
    public class MyClass2
    {
    }
}