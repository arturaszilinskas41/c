﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstDemo
{
    class ConstExample
    {
        //we need to assign a value to the const variable
        //at the time of const variable declaration else it will
        //give compile time error
        public const int number = 5;
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Const variables are static in nature
            //so we can access them by using class name 
            Console.WriteLine(ConstExample.number);

            //We can also declare constant variable within a function
            const int no = 10;
            Console.WriteLine(no);

            //Once after declaration we cannot change the value 
            //of a constant variable. so the below live gives error
            //no = 20;

            Console.WriteLine("Press any key to exist.");
            Console.ReadLine();
        }
    }
}