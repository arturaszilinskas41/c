﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibilityDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> foods = new List<string>();
            foods.Add("sandwitch");
            foods.Add("sandwitch");
            foods.Add("letus");
            foods.Add("sandwitch");

            Refrigerator refrigerator = new Refrigerator();
            refrigerator.CheckRefrigerator(foods);

            Console.Read();
        }
    }
    public abstract class Handler
    {
        public Handler nextHendler;
        public void NextHandler(Handler nextHnadler)
        {
            this.nextHendler = nextHnadler;
        }
        public abstract void FoodDestination(List<string> foods);
    }

    public class SandwitchHandler : Handler
    {
        public override void FoodDestination(List<string> foods)
        {
            for (int i = foods.Count - 1; i >= 0; i--)
            {
                if (foods[i] == "sandwitch") { 
                    Console.WriteLine("Eat sandwitch");
                    foods.RemoveAt(i);
                }
            }

            if (foods.Count != 0) {
                this.nextHendler.FoodDestination(foods);
            }
        }
    }

    public class LetusHandler : Handler
    {
        public override void FoodDestination(List<string> foods)
        {
            for (int i = foods.Count - 1; i >= 0; i--)
            {
                if (foods[i] == "letus")
                {
                    Console.WriteLine("Leave for ageing process");
                    foods.RemoveAt(i);
                }
            }

            if (foods.Count != 0)
            {
                this.nextHendler.FoodDestination(foods);
            }
        }
    }



    public class Refrigerator
    {
        private Handler SandwitchHandler = new SandwitchHandler();
        public Refrigerator()
        {
            SandwitchHandler.NextHandler(new LetusHandler());
        }
        public void CheckRefrigerator(List<string> foods)
        {
            this.SandwitchHandler.FoodDestination(foods);
        }
    }
}
