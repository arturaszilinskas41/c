﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ISubject subject = new Subject();

            subject.AddObserver(new UserObserver("user_name"));
            subject.AddObserver(new UserObserver("user_name1"));
            subject.AddObserver(new UserObserver("user_name2"));

            subject.NotifyObservers("Some random message");

            Console.ReadLine();
        }

        public interface ISubject
        {
            void AddObserver(IObserver observer);
            void RemoveObserver(IObserver observer);
            void NotifyObservers(string message);
        }

        public class Subject : ISubject
        {
            private List<IObserver> Observers = new List<IObserver>();
            public void AddObserver(IObserver observer)
            {
                Observers.Add(observer);
            }
            public void RemoveObserver(IObserver observer)
            {
                Observers.Remove(observer);
            }
            public void NotifyObservers(string message)
            {
                foreach(IObserver observer in Observers)
                {
                    observer.SendMessage(message);
                }
            }
        }

        public interface IObserver
        {
            void SendMessage(string message);
        }

        public class UserObserver : IObserver
        {
            private string UserName;
            public UserObserver(string userName)
            {
                UserName = userName;
            }
            public void SendMessage(string message)
            {
                Console.WriteLine("Sending {0} to {1}", message, UserName);
            }
        }
    }
}
