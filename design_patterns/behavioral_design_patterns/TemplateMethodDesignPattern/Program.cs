﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethodDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ProgramTemplate programTemplate = new PhpProgram();
            programTemplate.WriteAProgram();

            programTemplate = new CSharpProgram();
            programTemplate.WriteAProgram();

            Console.Read();
        }
    }

    public abstract class ProgramTemplate
    {
        public void WriteAProgram()
        {
            this.OpenEditor();
            this.WriteCode();
        }

        protected abstract void OpenEditor();
        protected abstract void WriteCode();
    }

    public class PhpProgram : ProgramTemplate { 
        protected override void OpenEditor()
        {
            Console.WriteLine("Opening PHP storm");
        }
        protected override void WriteCode()
        {
            Console.WriteLine("Write upload cat image to browser program");
        }
    }

    public class CSharpProgram : ProgramTemplate
    {
        protected override void OpenEditor()
        {
            Console.WriteLine("Opening Visual Studio");
        }
        protected override void WriteCode()
        {
            Console.WriteLine("Write a template abstarct class");
        }
    }
}
