﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            string question = Console.ReadLine();
            var interviewQuestion = new InterviewQuestion(question);
            string correctAnswer = interviewQuestion.CorrectAnswer();
            string wrongAnswer = interviewQuestion.WrongAnswer();
            Console.WriteLine("Correct answer: {0}, Wrong answer: {1}", correctAnswer, wrongAnswer);
            Console.ReadLine();
        }
    }

    public class InterviewQuestion : InterviewQuestionInterface
    {
        private InterviewQuestionInterface interviewQuestion;

        public InterviewQuestion(string interviewType)
        {
            switch (interviewType)
            {
                case "Git":
                    this.interviewQuestion = new GitQuestion();
                    break;
                case "DesignPatterns":
                    this.interviewQuestion = new DesignPatternsQuestion();
                    break;
                default:
                    this.interviewQuestion = new DefaultQuestion();
                    break;
            }
                
        }
        public string CorrectAnswer()
        {
            return this.interviewQuestion.CorrectAnswer();
        }
        public string WrongAnswer()
        {
            return this.interviewQuestion.WrongAnswer();
        }
    }

    public interface InterviewQuestionInterface {
        string WrongAnswer();
        string CorrectAnswer();
    }

    public class GitQuestion : InterviewQuestionInterface
    {
        public string CorrectAnswer()
        {
            return "Force";
        }

        public string WrongAnswer() {
            return "Cherry pick";
        }
    }

    public class DesignPatternsQuestion : InterviewQuestionInterface
    {
        public string CorrectAnswer()
        {
            return "Lazy factory visitor";
        }

        public string WrongAnswer()
        {
            return "Builders Dependency Principle";
        }
    }

    public class DefaultQuestion : InterviewQuestionInterface
    {
        public string CorrectAnswer()
        {
            return "Garbage collector";
        }

        public string WrongAnswer()
        {
            return "Fluent resposability";
        }
    }

}
