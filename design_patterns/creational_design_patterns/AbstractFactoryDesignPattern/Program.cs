﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            CreatureFactory creatureFactory = CreatureFactory.CreateCreatureFactory("Macro");
            CreatureInterface urMom = creatureFactory.GetCreature("UrMom");
            Console.WriteLine("urMom:{0}", urMom.Speak());


            CreatureInterface babyElephant = creatureFactory.GetCreature("BabyElephant");
            Console.WriteLine("babyElephant:{0}", babyElephant.Speak());


            Console.Read();
        }

        public abstract class CreatureFactory{

            public abstract CreatureInterface GetCreature(string creatureType);

            public static CreatureFactory CreateCreatureFactory(string factoryType)
            {
                if (factoryType == "Macro")
                {
                    return new MacroCreatureFactory();
                }
                else if (factoryType == "Micro")
                {
                    return new MicroCreatureFactory();
                }
                else
                {
                    return null;
                }
            }
        }

        public class  MacroCreatureFactory: CreatureFactory
        {
            public override CreatureInterface GetCreature(string creatureType)
            {
                if (creatureType == "BabyElephant")
                {
                    return new BabyElephant();
                }
                else if(creatureType == "UrMom")
                {
                    return new UrMom();
                }
                else
                {
                    return null;
                }
            }
        }

        public class MicroCreatureFactory : CreatureFactory
        {
            public override CreatureInterface GetCreature(string creatureType)
            {
                if (creatureType == "Vagatarian")
                {
                    return new Vagatarian();
                }
                else
                {
                    return null;
                }
            }
        }

        public interface CreatureInterface
        {
            string Speak();
        }

        public class BabyElephant : CreatureInterface
        {
            public string Speak()
            {
                return "Bethoven symphony";
            }
        }

        public class UrMom : CreatureInterface
        {
            public string Speak()
            {
                return "****";
            }
        }

        public class Vagatarian : CreatureInterface
        {
            public string Speak()
            {
                return "some fancy swag dab gibberish";
            }
        }
    }
}
