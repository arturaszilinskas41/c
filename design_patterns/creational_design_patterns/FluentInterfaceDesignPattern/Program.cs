﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentInterfaceDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            FluentEmployee fluentEmployee = new FluentEmployee();
            fluentEmployee
                .GiveEmplyeeAName("Elf")
                .EmployeeBossName("Santa")
                .GiveEmployeeAJob("Penguin Sheperd");
            Emloyee emloyee = fluentEmployee.GetEmployee();
            Console.WriteLine("EmplyeeName:{0}", emloyee.Name);
            Console.WriteLine("EmplyeeBoss:{0}", emloyee.Boss);
            Console.WriteLine("EmplyeeJob:{0}", emloyee.Job);

            Console.Read();
        }
    }

    public class Emloyee
    {
        public string Name { set; get; }
        public string Location { set; get; }
        public string Boss { set; get; }
        public string Job { set; get; }

    }

    public class FluentEmployee
    {
        private Emloyee emloyee;

        public FluentEmployee()
        {
            this.emloyee = new Emloyee();
        }
        public Emloyee GetEmployee()
        {
            return this.emloyee;
        }
        public FluentEmployee GiveEmplyeeAName(string name)
        {
            this.emloyee.Name = name;
            return this;
        }
        public FluentEmployee EmployeeBossName(string boss)
        {
            this.emloyee.Boss = boss;
            return this;
        }
        public FluentEmployee GiveEmployeeAJob(string job)
        {
            this.emloyee.Job = job;
            return this;
        }

    }
}
