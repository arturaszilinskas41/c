﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderDesignPattern2
{
    class Program
    {
        static void Main(string[] args)
        {
            FoodDirector foodDirector = new FoodDirector();
            Food SeaWeedPrincles = foodDirector.MakeFood(new SeaWeedPrinclesBuilder());
            SeaWeedPrincles.PrintFood();

            Food BubleSqueak = foodDirector.MakeFood(new BubleSqueakBuilder());
            BubleSqueak.PrintFood();

            Console.Read();
        }
    }

    public class Food
    {
        public int Potato { set; get; }
        public int Onion { set; get; }
        public int Carrot { set; get; }
        public int Salt { set; get; }
        public int SeaWeed { set; get; }
        public string Name { set; get; }
        public int Vegetarian { set; get; }

        public void PrintFood() {
            Console.WriteLine("Name:"+this.Name);
            Console.WriteLine("Potato:"+this.Potato);
            Console.WriteLine("Onion:"+this.Onion);
            Console.WriteLine("Carrot:"+this.Carrot);
            Console.WriteLine("Salt:"+this.Salt);
            Console.WriteLine("SeaWeed:"+this.SeaWeed);
            Console.WriteLine("Vegetarian:"+this.Vegetarian);
            Console.WriteLine("--");
        }
    }

    public abstract class FoodBuilder {
        protected Food foodObject;
        public void SetFoodObject()
        {
            this.foodObject = new Food();
        }

        public Food GetFoodObject()
        {
            return this.foodObject;
        }

        public abstract void SetPotato();
        public abstract void SetSeaWeed();
        public abstract void SetOnion();
        public abstract void SetCarrot();
        public abstract void SetSalt();
        public abstract void SetName();
        public abstract void SetVegetarian();
    }

    public class SeaWeedPrinclesBuilder : FoodBuilder
    {
        public override void SetPotato()
        {
            this.foodObject.Potato = 3;
        }
        public override void SetSeaWeed()
        {
            this.foodObject.SeaWeed = 1;
        }
        public override void SetOnion()
        {
            this.foodObject.Onion = 0;
        }
        public override void SetCarrot()
        {
            this.foodObject.Carrot = 0;
        }
        public override void SetSalt()
        {
            this.foodObject.Salt = 1;
        }
        public override void SetName()
        {
            this.foodObject.Name = "SeaWeedPrincles";
        }
        public override void SetVegetarian()
        {
            this.foodObject.Vegetarian = 0;
        }
    }
    public class BubleSqueakBuilder : FoodBuilder
    {
        public override void SetPotato()
        {
            this.foodObject.Potato = 2;
        }
        public override void SetSeaWeed()
        {
            this.foodObject.SeaWeed = 0;
        }
        public override void SetOnion()
        {
            this.foodObject.Onion = 1;
        }
        public override void SetCarrot()
        {
            this.foodObject.Carrot = 2;
        }
        public override void SetSalt()
        {
            this.foodObject.Salt = 1;
        }
        public override void SetName()
        {
            this.foodObject.Name = "BubleSqueak";
        }
        public override void SetVegetarian()
        {
            this.foodObject.Vegetarian = 1;
        }
    }

    public class FoodDirector
    {
        public Food MakeFood(FoodBuilder foodBuilder)
        {
            foodBuilder.SetFoodObject();

            foodBuilder.SetPotato();
            foodBuilder.SetSeaWeed();
            foodBuilder.SetOnion();
            foodBuilder.SetCarrot();
            foodBuilder.SetSalt();
            foodBuilder.SetName();
            foodBuilder.SetVegetarian();

            return foodBuilder.GetFoodObject();
        }
    }

}
