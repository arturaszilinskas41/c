﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderDesignPattern
{

    class Program
    {
        static void Main(string[] args)
        {
            MemeDirector memeDirector = new MemeDirector();
            Meme meme = memeDirector.MakeMeme(new WallMeme());
            meme.DisplayMeme();
            Console.ReadLine();
        }
    }

    public class Meme{
        public string MemeTitle { set; get; }
        public string MemeBody { set; get; }
        public string MemeImage { set; get; }

        public void DisplayMeme()
        {
            Console.WriteLine("MemeTitle: {0}", MemeTitle);
            Console.WriteLine("MemeBody: {0}", MemeBody);
            Console.WriteLine("MemeImage: {0}", MemeImage);
        }
    }



    public abstract class MemeBuilder{
        protected Meme memeObject;
        public abstract void SetTitle();
        public abstract void SetBody();
        public abstract void SetImage();
        public void CreateMeme()
        {
            this.memeObject = new Meme();
        }
        public Meme GetMeme()
        {
            return this.memeObject;
        }
    }

    public class MemeDirector
    {
        public Meme MakeMeme(MemeBuilder memeBuilder)
        {
            memeBuilder.CreateMeme();
            memeBuilder.SetTitle();
            memeBuilder.SetBody();
            memeBuilder.SetImage();
            return memeBuilder.GetMeme();
        }
    }

    public class WallMeme : MemeBuilder
    {
        public override void SetTitle()
        {
            memeObject.MemeTitle = "Build A Wall";
        }
        public override void SetBody()
        {
            memeObject.MemeBody = "Build deadline is here";
        }
        public override void SetImage()
        {
            memeObject.MemeImage = "./ChinaSatilite.jpeg";
        }

    }



}
