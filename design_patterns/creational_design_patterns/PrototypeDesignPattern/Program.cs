﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new Employee();
            emp1.Name = "Elf";
            emp1.Job = "PenguinSheperd";

            Employee emp2 = emp1.GetClone();
            emp2.Name = "Gnome";

            Console.WriteLine("emp1 name:{0}", emp1.Name);
            Console.WriteLine("emp1 job:{0}", emp1.Job);
            Console.WriteLine("emp2 name:{0}", emp2.Name);
            Console.WriteLine("emp2 job:{0}", emp2.Job);
            Console.Read();

        }

        public class Employee
        {
            public string Name { set; get; }
            public string Job { set; get; }

            public Employee GetClone()
            {
                return (Employee)this.MemberwiseClone();
            }
        }
    }
}
