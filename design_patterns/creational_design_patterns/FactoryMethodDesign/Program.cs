﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodDesign
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkerFactory AIWorkerFactory = new ArtificialIntelligenceWorkerFactory();
            WorkerInterface AIWorker = AIWorkerFactory.CreateWorker();

            Console.WriteLine("AIWorker name: {0}" , AIWorker.GetName());
            Console.WriteLine("AIWorker status: {0}" , AIWorker.GetStatus());

            WorkerInterface factoryWorker = new FactoryWorkerFactory().CreateWorker();

            Console.WriteLine("AIWorker name: {0}", factoryWorker.GetName());
            Console.WriteLine("AIWorker status: {0}", factoryWorker.GetStatus());

            Console.ReadLine();
        }

        public abstract class WorkerFactory
        {
            protected abstract WorkerInterface MakeWorker();

            public WorkerInterface CreateWorker()
            {
                return this.MakeWorker();
            }
        }

        public class ArtificialIntelligenceWorkerFactory : WorkerFactory
        {
            protected override WorkerInterface MakeWorker()
            {
                return new ArtificialIntelligenceWorker();
            }
        }

        public class FactoryWorkerFactory : WorkerFactory
        {
            protected override WorkerInterface MakeWorker()
            {
                return new FactoryWorker();
            }
        }

        public interface WorkerInterface
        {
            string GetName();
            string GetStatus();
        }

        public class ArtificialIntelligenceWorker: WorkerInterface
        {
            public string GetName()
            {
                return "BASD<O233";
            }
            public string GetStatus()
            {
                return "Active";
            }
        }

        public class FactoryWorker: WorkerInterface
        {
            public string GetName()
            {
                return "Staska";
            }
            public string GetStatus()
            {
                return "Drunk";
            }
        }

    }
}
