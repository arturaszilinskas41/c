﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace MVCUsingUnity.Controllers
{

    public class InstitutionController : Controller
    {
        private IInstitutionService institutionService;
        public InstitutionController(IInstitutionService institutionService)
        {
            var container = new UnityContainer();
            container.Resolve<IInstitutionService>;
            this.institutionService = institutionService;
        }
        // GET: Institution
        public ActionResult Index()
        {
            return View(this.institutionService.GetInstitutionByID(1));
        }
    }
}