﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionByConstructorDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            UserBL userBL = new UserBL();
            List<User> users = userBL.SelectUsers();

            foreach (User user in users)
            {
                Console.WriteLine("user name: {0}", user.Name);
            }

            Console.Read();
        }
    }
    public class User
    {
        public string Name { get; set; }
    }
    public interface IUserDataAcessLayer {
        List<User> SelectUsers();
    }
    public class UserDAL : IUserDataAcessLayer
    {
        public List<User> SelectUsers()
        {
            List<User> UsersList = new List<User>();
            //Get the Employees from the Database
            //for now we are hard coded the employees
            UsersList.Add(new User() { Name = "Name1" });
            UsersList.Add(new User() { Name = "Name2" });
            UsersList.Add(new User() { Name = "Name3" });
            return UsersList;
        }
    }

    public class UserBL {

        private UserDAL UserDAL;
        public UserBL()
        {
            this.UserDAL = new UserDAL();
        }
        public List<User> SelectUsers()
        {
            return this.UserDAL.SelectUsers();
        }
    }
}
